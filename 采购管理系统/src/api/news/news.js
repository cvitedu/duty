import request from '@/utils/request'

// 查询热门新闻列表
export function listNews(query) {
  return request({
    url: '/news/news/list',
    method: 'get',
    params: query
  })
}

// 查询热门新闻详细
export function getNews(id) {
  return request({
    url: '/news/news/' + id,
    method: 'get'
  })
}

// 新增热门新闻
export function addNews(data) {
  return request({
    url: '/news/news',
    method: 'post',
    data: data
  })
}

// 修改热门新闻
export function updateNews(data) {
  return request({
    url: '/news/news',
    method: 'put',
    data: data
  })
}

// 删除热门新闻
export function delNews(id) {
  return request({
    url: '/news/news/' + id,
    method: 'delete'
  })
}
