import request from '@/utils/request'

// 查询用户关注列表
export function listUserFocus(query) {
  return request({
    url: '/UserFocus/UserFocus/list',
    method: 'get',
    params: query
  })
}

// 查询用户关注详细
export function getUserFocus(id) {
  return request({
    url: '/UserFocus/UserFocus/' + id,
    method: 'get'
  })
}

// 新增用户关注
export function addUserFocus(data) {
  return request({
    url: '/UserFocus/UserFocus',
    method: 'post',
    data: data
  })
}

// 修改用户关注
export function updateUserFocus(data) {
  return request({
    url: '/UserFocus/UserFocus',
    method: 'put',
    data: data
  })
}

// 删除用户关注
export function delUserFocus(data) {
  return request({
    url: '/UserFocus/UserFocus' ,
    method: 'delete',
    data: data
  })
}
