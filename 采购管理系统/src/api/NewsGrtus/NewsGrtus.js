import request from '@/utils/request'

// 查询用户点赞id列表
export function listNewsGrtus(query) {
  return request({
    url: '/NewsGrtus/NewsGrtus/list',
    method: 'get',
    params: query
  })
}

// 查询用户点赞id详细
export function getNewsGrtus(id) {
  return request({
    url: '/NewsGrtus/NewsGrtus/' + id,
    method: 'get'
  })
}

// 新增用户点赞id
export function addNewsGrtus(data) {
  return request({
    url: '/NewsGrtus/NewsGrtus',
    method: 'post',
    data: data
  })
}

// 修改用户点赞id
export function updateNewsGrtus(data) {
  return request({
    url: '/NewsGrtus/NewsGrtus',
    method: 'put',
    data: data
  })
}

// 删除用户点赞id
export function delNewsGrtus(data) {
  return request({
    url: '/NewsGrtus/NewsGrtus' ,
    method: 'delete',
    data: data

  })
}
