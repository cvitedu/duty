import request from '@/utils/request'

// 查询搜索单列表
export function listSearch(query) {
  return request({
    url: '/Search/Search/list',
    method: 'get',
    params: query
  })
}
export function getMySearch(query) {
  return request({
    url: '/Search/Search',
    method: 'get',
    params: query
  })
}
// 查询搜索单详细
export function getSearch(id) {
  return request({
    url: '/Search/Search/' + id,
    method: 'get'
  })
}

// 新增搜索单
export function addSearch(data) {
  return request({
    url: '/Search/Search',
    method: 'post',
    data: data
  })
}

// 修改搜索单
export function updateSearch(data) {
  return request({
    url: '/Search/Search',
    method: 'put',
    data: data
  })
}

// 删除搜索单
export function delSearch(id) {
  return request({
    url: '/Search/Search/' + id,
    method: 'delete'
  })
}

export function listHotSearch(query) {
  return request({
    url: '/news/news/hot/category/list',
    method: 'get',
    params: query
  })
}

export function listHot(query) {
  return request({
    url: '/news/news/hot/list',
    method: 'get',
    params: query
  })
}