import request from '@/utils/request'

// 查询采购出行助手值机列表
export function listCaiGouChuXingZhuShouZhiJi(query) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouZhiJi/list',
    method: 'get',
    params: query
  })
}

// 查询采购出行助手值机详细
export function getCaiGouChuXingZhuShouZhiJi(id) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouZhiJi/' + id,
    method: 'get'
  })
}

// 新增采购出行助手值机
export function addCaiGouChuXingZhuShouZhiJi(data) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouZhiJi',
    method: 'post',
    data: data
  })
}

// 修改采购出行助手值机
export function updateCaiGouChuXingZhuShouZhiJi(data) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouZhiJi',
    method: 'put',
    data: data
  })
}

// 删除采购出行助手值机
export function delCaiGouChuXingZhuShouZhiJi(id) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouZhiJi/' + id,
    method: 'delete'
  })
}
