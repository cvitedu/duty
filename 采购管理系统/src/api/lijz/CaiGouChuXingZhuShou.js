import request from '@/utils/request'

// 查询采购出行助手列表
export function listCaiGouChuXingZhuShou(query) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShou/list',
    method: 'get',
    params: query
  })
}

// 查询采购出行助手详细
export function getCaiGouChuXingZhuShou(id) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShou/' + id,
    method: 'get'
  })
}

// 新增采购出行助手
export function addCaiGouChuXingZhuShou(data) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShou',
    method: 'post',
    data: data
  })
}

// 修改采购出行助手
export function updateCaiGouChuXingZhuShou(data) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShou',
    method: 'put',
    data: data
  })
}

// 删除采购出行助手
export function delCaiGouChuXingZhuShou(id) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShou/' + id,
    method: 'delete'
  })
}
