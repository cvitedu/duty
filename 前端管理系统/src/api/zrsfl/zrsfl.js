import request from '@/utils/request'

// 查询值日生分类列表
export function listZrsfl(query) {
  return request({
    url: '/zrsfl/zrsfl/list',
    method: 'get',
    params: query
  })
}

// 查询值日生分类详细
export function getZrsfl(id) {
  return request({
    url: '/zrsfl/zrsfl/' + id,
    method: 'get'
  })
}

// 新增值日生分类
export function addZrsfl(data) {
  return request({
    url: '/zrsfl/zrsfl',
    method: 'post',
    data: data
  })
}

// 修改值日生分类
export function updateZrsfl(data) {
  return request({
    url: '/zrsfl/zrsfl',
    method: 'put',
    data: data
  })
}

// 删除值日生分类
export function delZrsfl(id) {
  return request({
    url: '/zrsfl/zrsfl/' + id,
    method: 'delete'
  })
}
