import request from '@/utils/request'

// 查询公开课程列表
export function listGkk(query) {
  return request({
    url: '/gkk/gkk/list',
    method: 'get',
    params: query
  })
}

// 查询公开课程详细
export function getGkk(id) {
  return request({
    url: '/gkk/gkk/' + id,
    method: 'get'
  })
}

// 新增公开课程
export function addGkk(data) {
  return request({
    url: '/gkk/gkk',
    method: 'post',
    data: data
  })
}

// 修改公开课程
export function updateGkk(data) {
  return request({
    url: '/gkk/gkk',
    method: 'put',
    data: data
  })
}

// 删除公开课程
export function delGkk(id) {
  return request({
    url: '/gkk/gkk/' + id,
    method: 'delete'
  })
}
