import request from '@/utils/request'

// 查询值日日期列表
export function listDutyDate(query) {
  return request({
    url: '/DutyDate/DutyDate/list',
    method: 'get',
    params: query
  })
}

// 查询值日日期详细
export function getDutyDate(id) {
  return request({
    url: '/DutyDate/DutyDate/' + id,
    method: 'get'
  })
}

// 新增值日日期
export function addDutyDate(data) {
  return request({
    url: '/DutyDate/DutyDate',
    method: 'post',
    data: data
  })
}

// 修改值日日期
export function updateDutyDate(data) {
  return request({
    url: '/DutyDate/DutyDate',
    method: 'put',
    data: data
  })
}

// 删除值日日期
export function delDutyDate() {
  return request({
    url: '/DutyDate/DutyDate' ,
    method: 'delete'
  })
}
