import request from '@/utils/request'

// 查询分类名称列表
export function listNewsCategory(query) {
  return request({
    url: '/NewsCategory/NewsCategory/list',
    method: 'get',
    params: query
  })
}


export function listAll() {
  return request({
    url: '/NewsCategory/NewsCategory/list',
    method: 'get'
  })
}



// 查询分类名称详细
export function getNewsCategory(id) {
  return request({
    url: '/NewsCategory/NewsCategory/' + id,
    method: 'get'
  })
}

// 新增分类名称
export function addNewsCategory(data) {
  return request({
    url: '/NewsCategory/NewsCategory',
    method: 'post',
    data: data
  })
}

// 修改分类名称
export function updateNewsCategory(data) {
  return request({
    url: '/NewsCategory/NewsCategory',
    method: 'put',
    data: data
  })
}

// 删除分类名称
export function delNewsCategory(id) {
  return request({
    url: '/NewsCategory/NewsCategory/' + id,
    method: 'delete'
  })
}
