import request from '@/utils/request'

// 查询采购出行助手航班列表
export function listCaiGouChuXingZhuShouHangBan(query) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouHangBan/list',
    method: 'get',
    params: query
  })
}

// 查询采购出行助手航班详细
export function getCaiGouChuXingZhuShouHangBan(id) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouHangBan/' + id,
    method: 'get'
  })
}

// 新增采购出行助手航班
export function addCaiGouChuXingZhuShouHangBan(data) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouHangBan',
    method: 'post',
    data: data
  })
}

// 修改采购出行助手航班
export function updateCaiGouChuXingZhuShouHangBan(data) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouHangBan',
    method: 'put',
    data: data
  })
}

// 删除采购出行助手航班
export function delCaiGouChuXingZhuShouHangBan(id) {
  return request({
    url: '/lijz/CaiGouChuXingZhuShouHangBan/' + id,
    method: 'delete'
  })
}
