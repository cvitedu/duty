package com.ruoyi.dutydate.service;

import java.util.List;
import com.ruoyi.dutydate.domain.DutyDate;

/**
 * 值日日期Service接口
 * 
 * @author 小宋
 * @date 2024-04-16
 */
public interface IDutyDateService 
{
    /**
     * 查询值日日期
     * 
     * @param id 值日日期主键
     * @return 值日日期
     */
    public DutyDate selectDutyDateById(Long id);

    /**
     * 查询值日日期列表
     *
     * @param dutyDate 值日日期
     * @return 值日日期集合
     */
    public List<DutyDate> selectDutyDateList(DutyDate dutyDate);

    /**
     * 新增值日日期
     * 
     * @param dutyDate 值日日期
     * @return 结果
     */
    public int insertDutyDate(DutyDate dutyDate);

    /**
     * 修改值日日期
     * 
     * @param dutyDate 值日日期
     * @return 结果
     */
    public int updateDutyDate(DutyDate dutyDate);

    /**
     * 批量删除值日日期
     * 
     * @param ids 需要删除的值日日期主键集合
     * @return 结果
     */
    public int deleteDutyDateByIds();

    /**
     * 删除值日日期信息
     * 
     * @param id 值日日期主键
     * @return 结果
     */
    public int deleteDutyDateById(Long id);
}
