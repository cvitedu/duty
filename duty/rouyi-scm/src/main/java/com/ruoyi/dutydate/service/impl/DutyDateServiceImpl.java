package com.ruoyi.dutydate.service.impl;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import com.ruoyi.dutydate.mapper.DutyDateMapper;
import com.ruoyi.dutydate.domain.DutyDate;
import com.ruoyi.dutydate.service.IDutyDateService;

/**
 * 值日日期Service业务层处理
 * 
 * @author 小宋
 * @date 2024-04-16
 */
@Service
public class DutyDateServiceImpl implements IDutyDateService 
{
    @Autowired
    private DutyDateMapper dutyDateMapper;

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * 查询值日日期
     * 
     * @param id 值日日期主键
     * @return 值日日期
     */
    @Override
    public DutyDate selectDutyDateById(Long id)
    {
        return dutyDateMapper.selectDutyDateById(id);
    }

    /**
     * 查询值日日期列表
     *
     * @param dutyDate 值日日期
     * @return 值日日期
     */
    @Override
    public List<DutyDate> selectDutyDateList(DutyDate dutyDate)
    {
        return dutyDateMapper.selectDutyDateList(dutyDate);
    }

    /**
     * 新增值日日期
     * 
     * @param dutyDate 值日日期
     * @return 结果
     */
    @Override
    public int insertDutyDate(DutyDate dutyDate)
    {
        //删除对用的值日生
        dutyDateMapper.deleteCategotys(dutyDate.getCategoryIds());
        ArrayList<DutyDate> list = new ArrayList<>();
        DutyDate myDuty = new DutyDate();
        for (Long categoryId : dutyDate.getCategoryIds()) {
            SysUser user = new SysUser();
            user.setCategory(categoryId);
            List<SysUser> users = sysUserMapper.selectUserList(user);
            if (users.size() == 0) {
                continue;
            }
            //循环90
            int userIndex = 0;
            for (int i = 1; i < 90; i++) {
                // 获取当前日期
                LocalDate currentDate = LocalDate.now();

                // 计算日期时间值
                LocalDate dateTime = currentDate.plusDays(i);

                if (dateTime.getDayOfWeek() == DayOfWeek.SATURDAY || dateTime.getDayOfWeek() == DayOfWeek.SUNDAY) {
                    continue;
                }

                // 将LocalDate转换为Date
                ZoneId defaultZoneId = ZoneId.systemDefault();
                Date date = Date.from(dateTime.atStartOfDay(defaultZoneId).toInstant());

                myDuty.setUserId(users.get(userIndex).getUserId());
                myDuty.setCategoryId(categoryId);
                myDuty.setDatetime(date);

                dutyDateMapper.insertDutyDate(myDuty);
                userIndex++;
                if (userIndex == users.size()) {
                    userIndex = 0;
                }
            }

        }
        return 200;
    }

    /**
     * 修改值日日期
     * 
     * @param dutyDate 值日日期
     * @return 结果
     */
    @Override
    public int updateDutyDate(DutyDate dutyDate)
    {
        return dutyDateMapper.updateDutyDate(dutyDate);
    }


    @Override
    public int deleteDutyDateByIds()
    {
        return dutyDateMapper.deleteDutyDateByIds();
    }

    /**
     * 删除值日日期信息
     * 
     * @param id 值日日期主键
     * @return 结果
     */
    @Override
    public int deleteDutyDateById(Long id)
    {
        return dutyDateMapper.deleteDutyDateById(id);
    }
}
