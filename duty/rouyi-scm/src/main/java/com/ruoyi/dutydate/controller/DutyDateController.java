package com.ruoyi.dutydate.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.dutydate.domain.DutyDate;
import com.ruoyi.dutydate.service.IDutyDateService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 值日日期Controller
 * 
 * @author 小宋
 * @date 2024-04-16
 */
@RestController
@RequestMapping("/DutyDate/DutyDate")
public class DutyDateController extends BaseController
{
    @Autowired
    private IDutyDateService dutyDateService;

    /**
     * 查询值日日期列表
     */
    @PreAuthorize("@ss.hasPermi('DutyDate:DutyDate:list')")
    @GetMapping("/list")
    public TableDataInfo list(DutyDate dutyDate)
    {
        startPage();
        ArrayList<DutyDate> dutyDates = new ArrayList<>();
        if (dutyDate.getCategoryIds() != null) {
            for (Long categoryId : dutyDate.getCategoryIds()) {
                DutyDate duty;
                duty = dutyDate;
                duty.setCategoryId(Long.valueOf(categoryId));
                dutyDates.addAll(dutyDateService.selectDutyDateList(duty));
            }
        }
        else {
            dutyDates.addAll(dutyDateService.selectDutyDateList(dutyDate)) ;
        }


        return getDataTable(dutyDates);
    }

    /**
     * 导出值日日期列表
     */
    @PreAuthorize("@ss.hasPermi('DutyDate:DutyDate:export')")
    @Log(title = "值日日期", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DutyDate dutyDate)
    {
        List<DutyDate> list = dutyDateService.selectDutyDateList(dutyDate);
        ExcelUtil<DutyDate> util = new ExcelUtil<DutyDate>(DutyDate.class);
        util.exportExcel(response, list, "值日日期数据");
    }

    /**
     * 获取值日日期详细信息
     */
    @PreAuthorize("@ss.hasPermi('DutyDate:DutyDate:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(dutyDateService.selectDutyDateById(id));
    }

    /**
     * 新增值日日期
     */
    @PreAuthorize("@ss.hasPermi('DutyDate:DutyDate:add')")
    @Log(title = "生成日期", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DutyDate dutyDate)
    {
        return toAjax(dutyDateService.insertDutyDate(dutyDate));
    }

    /**
     * 修改值日日期
     */
    @PreAuthorize("@ss.hasPermi('DutyDate:DutyDate:edit')")
    @Log(title = "值日日期", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DutyDate dutyDate)
    {
        return toAjax(dutyDateService.updateDutyDate(dutyDate));
    }

    /**
     * 删除值日日期
     */
    @PreAuthorize("@ss.hasPermi('DutyDate:DutyDate:remove')")
    @Log(title = "值日日期", businessType = BusinessType.DELETE)
	@DeleteMapping
    public AjaxResult remove()
    {
        return toAjax(dutyDateService.deleteDutyDateByIds());
    }
}
