package com.ruoyi.gkk.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.gkk.mapper.gkkMapper;
import com.ruoyi.gkk.domain.gkk;
import com.ruoyi.gkk.service.IgkkService;

/**
 * 公开课程Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-21
 */
@Service
public class gkkServiceImpl implements IgkkService 
{
    @Autowired
    private gkkMapper gkkMapper;

    /**
     * 查询公开课程
     * 
     * @param id 公开课程主键
     * @return 公开课程
     */
    @Override
    public gkk selectgkkById(Long id)
    {
        return gkkMapper.selectgkkById(id);
    }

    /**
     * 查询公开课程列表
     * 
     * @param gkk 公开课程
     * @return 公开课程
     */
    @Override
    public List<gkk> selectgkkList(gkk gkk)
    {
        return gkkMapper.selectgkkList(gkk);
    }

    /**
     * 新增公开课程
     * 
     * @param gkk 公开课程
     * @return 结果
     */
    @Override
    public int insertgkk(gkk gkk)
    {
        return gkkMapper.insertgkk(gkk);
    }

    /**
     * 修改公开课程
     * 
     * @param gkk 公开课程
     * @return 结果
     */
    @Override
    public int updategkk(gkk gkk)
    {
        return gkkMapper.updategkk(gkk);
    }

    /**
     * 批量删除公开课程
     * 
     * @param ids 需要删除的公开课程主键
     * @return 结果
     */
    @Override
    public int deletegkkByIds(Long[] ids)
    {
        return gkkMapper.deletegkkByIds(ids);
    }

    /**
     * 删除公开课程信息
     * 
     * @param id 公开课程主键
     * @return 结果
     */
    @Override
    public int deletegkkById(Long id)
    {
        return gkkMapper.deletegkkById(id);
    }
}
