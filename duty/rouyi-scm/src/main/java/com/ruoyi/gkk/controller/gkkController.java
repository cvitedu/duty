package com.ruoyi.gkk.controller;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.dutydate.domain.DutyDate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.gkk.domain.gkk;
import com.ruoyi.gkk.service.IgkkService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公开课程Controller
 *
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/gkk/gkk")
public class gkkController extends BaseController
{
    @Autowired
    private IgkkService gkkService;

    /**
     * 查询公开课程列表
     */
    @PreAuthorize("@ss.hasPermi('gkk:gkk:list')")
    @GetMapping("/list")
    public TableDataInfo list(gkk gkks)
    {
        startPage();
        ArrayList<gkk> gkkss = new ArrayList<>();
        if (gkks.getCategoryIds() != null) {
            for (String category : gkks.getCategoryIds()) {
                gkk gk;
                gk = gkks;
                gk.setCategory(String.valueOf(category));
                gkkss.addAll(gkkService.selectgkkList(gk));
            }
        }
        else {
            gkkss.addAll(gkkService.selectgkkList(gkks)) ;
        }

        return getDataTable(gkkss);
    }

    /**
     * 导出公开课程列表
     */
    @PreAuthorize("@ss.hasPermi('gkk:gkk:export')")
    @Log(title = "公开课程", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, gkk gkk)
    {
        List<gkk> list = gkkService.selectgkkList(gkk);
        ExcelUtil<gkk> util = new ExcelUtil<gkk>(gkk.class);
        util.exportExcel(response, list, "公开课程数据");
    }

    /**
     * 获取公开课程详细信息
     */
    @PreAuthorize("@ss.hasPermi('gkk:gkk:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(gkkService.selectgkkById(id));
    }

    /**
     * 新增公开课程
     */
    @PreAuthorize("@ss.hasPermi('gkk:gkk:add')")
    @Log(title = "公开课程", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody gkk gkk)
    {
        return toAjax(gkkService.insertgkk(gkk));
    }

    /**
     * 修改公开课程
     */
    @PreAuthorize("@ss.hasPermi('gkk:gkk:edit')")
    @Log(title = "公开课程", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody gkk gkk)
    {
        return toAjax(gkkService.updategkk(gkk));
    }

    /**
     * 删除公开课程
     */
    @PreAuthorize("@ss.hasPermi('gkk:gkk:remove')")
    @Log(title = "公开课程", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(gkkService.deletegkkByIds(ids));
    }
}