package com.ruoyi.gkk.mapper;

import java.util.List;
import com.ruoyi.gkk.domain.gkk;

/**
 * 公开课程Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-21
 */
public interface gkkMapper 
{
    /**
     * 查询公开课程
     * 
     * @param id 公开课程主键
     * @return 公开课程
     */
    public gkk selectgkkById(Long id);

    /**
     * 查询公开课程列表
     * 
     * @param gkk 公开课程
     * @return 公开课程集合
     */
    public List<gkk> selectgkkList(gkk gkk);

    /**
     * 新增公开课程
     * 
     * @param gkk 公开课程
     * @return 结果
     */
    public int insertgkk(gkk gkk);

    /**
     * 修改公开课程
     * 
     * @param gkk 公开课程
     * @return 结果
     */
    public int updategkk(gkk gkk);

    /**
     * 删除公开课程
     * 
     * @param id 公开课程主键
     * @return 结果
     */
    public int deletegkkById(Long id);

    /**
     * 批量删除公开课程
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletegkkByIds(Long[] ids);
}
