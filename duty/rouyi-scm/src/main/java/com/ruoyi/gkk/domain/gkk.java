package com.ruoyi.gkk.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公开课程对象 scm_video
 *
 * @author ruoyi
 * @date 2024-04-25
 */
public class gkk extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String id;

    /** 课程视频 */
    @Excel(name = "课程视频")
    private String video;

    /** 课程标题 */
    @Excel(name = "课程标题")
    private String title;

    /** 课程学习量 */
    @Excel(name = "课程学习量")
    private String learning;

    /** 课程分类 */
    @Excel(name = "课程分类")
    private String category;

    /** 课程有效期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "课程有效期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date exDate;

    /** 授课类型 0:录播课 1:其他 */
    @Excel(name = "授课类型 0:录播课 1:其他")
    private String functionCategory;

    /** 学习人数 */
    @Excel(name = "学习人数")
    private String learningNum;

    /** 课程封面图 */
    @Excel(name = "课程封面图")
    private String videoPhoto;

    /** 排序 */
    private String px;

    /** 课程分类数组 */
    private List<String> categoryList;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setVideo(String video)
    {
        this.video = video;
    }

    public String getVideo()
    {
        return video;
    }
    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return title;
    }
    public void setLearning(String learning)
    {
        this.learning = learning;
    }

    public String getLearning()
    {
        return learning;
    }
    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getCategory()
    {
        return category;
    }
    public void setExDate(Date exDate)
    {
        this.exDate = exDate;
    }

    public Date getExDate()
    {
        return exDate;
    }
    public void setFunctionCategory(String functionCategory)
    {
        this.functionCategory = functionCategory;
    }

    public String getFunctionCategory()
    {
        return functionCategory;
    }
    public void setLearningNum(String learningNum)
    {
        this.learningNum = learningNum;
    }

    public String getLearningNum()
    {
        return learningNum;
    }
    public void setVideoPhoto(String videoPhoto)
    {
        this.videoPhoto = videoPhoto;
    }
//ids
    private String[] categoryIds;

    public String[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(String[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String getVideoPhoto()
    {
        return videoPhoto;
    }

    public String getPx() {
        return px;
    }

    public void setPx(String px) {
        this.px = px;
    }

    public List<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<String> categoryList) {
        this.categoryList = categoryList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("video", getVideo())
                .append("title", getTitle())
                .append("learning", getLearning())
                .append("remark", getRemark())
                .append("category", getCategory())
                .append("exDate", getExDate())
                .append("functionCategory", getFunctionCategory())
                .append("learningNum", getLearningNum())
                .append("videoPhoto", getVideoPhoto())
                .append("px", getPx())
                .append("categoryList", Arrays.toString(getCategoryList().toArray()))
                .toString();
    }
}