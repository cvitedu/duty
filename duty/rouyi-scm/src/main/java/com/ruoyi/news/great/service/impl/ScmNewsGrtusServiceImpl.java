package com.ruoyi.news.great.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.news.great.mapper.ScmNewsGrtusMapper;
import com.ruoyi.news.great.domain.ScmNewsGrtus;
import com.ruoyi.news.great.service.IScmNewsGrtusService;

/**
 * 用户点赞idService业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class ScmNewsGrtusServiceImpl implements IScmNewsGrtusService 
{
    @Autowired
    private ScmNewsGrtusMapper scmNewsGrtusMapper;

    /**
     * 查询用户点赞id
     * 
     * @param id 用户点赞id主键
     * @return 用户点赞id
     */
    @Override
    public ScmNewsGrtus selectScmNewsGrtusById(Long id)
    {
        return scmNewsGrtusMapper.selectScmNewsGrtusById(id);
    }

    /**
     * 查询用户点赞id列表
     * 
     * @param scmNewsGrtus 用户点赞id
     * @return 用户点赞id
     */
    @Override
    public List<ScmNewsGrtus> selectScmNewsGrtusList(ScmNewsGrtus scmNewsGrtus)
    {
        return scmNewsGrtusMapper.selectScmNewsGrtusList(scmNewsGrtus);
    }

    /**
     * 新增用户点赞id
     * 
     * @param scmNewsGrtus 用户点赞id
     * @return 结果
     */
    @Override
    public int insertScmNewsGrtus(ScmNewsGrtus scmNewsGrtus)
    {
        return scmNewsGrtusMapper.insertScmNewsGrtus(scmNewsGrtus);
    }

    /**
     * 修改用户点赞id
     * 
     * @param scmNewsGrtus 用户点赞id
     * @return 结果
     */
    @Override
    public int updateScmNewsGrtus(ScmNewsGrtus scmNewsGrtus)
    {
        return scmNewsGrtusMapper.updateScmNewsGrtus(scmNewsGrtus);
    }

    /**
     * 批量删除用户点赞id
     * 
     * @param ids 需要删除的用户点赞id主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsGrtusByIds(Long[] ids)
    {
        return scmNewsGrtusMapper.deleteScmNewsGrtusByIds(ids);
    }

    /**
     * 删除用户点赞id信息
     * 
     * @param id 用户点赞id主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsGrtusById(Long id)
    {
        return scmNewsGrtusMapper.deleteScmNewsGrtusById(id);
    }

    @Override
    public Long selectCountGreat(Long newId) {
        return scmNewsGrtusMapper.selectCountGreat(newId);
    }

    @Override
    public int deleteScmNewsGrtus(ScmNewsGrtus scmNewsGrtus) {
        return scmNewsGrtusMapper.deleteScmNewsGrtus(scmNewsGrtus);
    }
}
