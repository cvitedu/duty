package com.ruoyi.news.search.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ruoyi.news.domain.ScmNews;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.news.search.mapper.ScmNewsSearchMapper;
import com.ruoyi.news.search.domain.ScmNewsSearch;
import com.ruoyi.news.search.service.IScmNewsSearchService;

/**
 * 搜索单Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-21
 */
@Service
public class ScmNewsSearchServiceImpl implements IScmNewsSearchService 
{
    @Autowired
    private ScmNewsSearchMapper scmNewsSearchMapper;

    /**
     * 查询搜索单
     * 
     * @param id 搜索单主键
     * @return 搜索单
     */
    @Override
    public ScmNewsSearch selectScmNewsSearchById(Long id)
    {
        return scmNewsSearchMapper.selectScmNewsSearchById(id);
    }

    /**
     * 查询搜索单列表
     * 
     * @param scmNewsSearch 搜索单
     * @return 搜索单
     */
    @Override
    public List<ScmNewsSearch> selectScmNewsSearchList(ScmNewsSearch scmNewsSearch)
    {
        List<ScmNewsSearch> list = scmNewsSearchMapper.selectScmNewsSearchList(scmNewsSearch);
        //排序
        Collections.sort(list, new SearchComparator());
        return list;
    }

    /**
     * 新增搜索单
     * 
     * @param scmNewsSearch 搜索单
     * @return 结果
     */
    @Override
    public int insertScmNewsSearch(ScmNewsSearch scmNewsSearch)
    {
        return scmNewsSearchMapper.insertScmNewsSearch(scmNewsSearch);
    }

    /**
     * 修改搜索单
     * 
     * @param scmNewsSearch 搜索单
     * @return 结果
     */
    @Override
    public int updateScmNewsSearch(ScmNewsSearch scmNewsSearch)
    {
        return scmNewsSearchMapper.updateScmNewsSearch(scmNewsSearch);
    }

    /**
     * 批量删除搜索单
     * 
     * @param ids 需要删除的搜索单主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsSearchByIds(Long[] ids)
    {
        return scmNewsSearchMapper.deleteScmNewsSearchByIds(ids);
    }

    /**
     * 删除搜索单信息
     * 
     * @param id 搜索单主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsSearchById(Long id)
    {
        return scmNewsSearchMapper.deleteScmNewsSearchById(id);
    }

    @Override
    public int getMySearch(ScmNewsSearch scmNewsSearch) {
        //如果用户什么也没有传入就让其无功而返
        if (scmNewsSearch.getSearchName() == null || scmNewsSearch.getSearchName().equals("")) {
            return 0;
        }
        ScmNewsSearch search = scmNewsSearchMapper.selectMySearchList(scmNewsSearch);
        //如果用户查询的数据在数据库中存在 那么就让他的搜索数量加一
        if (search != null) {
            search.setSearchNum(String.valueOf(Integer.parseInt(search.getSearchNum())+1));
            return  scmNewsSearchMapper.updateScmNewsSearch(search);
        }
        else{
            //其他情况就让数据库新增这个搜索的title
            scmNewsSearch.setSearchNum("1");
            return  scmNewsSearchMapper.insertScmNewsSearch(scmNewsSearch);
        }
    }

    //排序方法
    private class SearchComparator implements Comparator<ScmNewsSearch> {
        @Override
        public int compare(ScmNewsSearch news1, ScmNewsSearch news2) {
            return Long.compare(Integer.parseInt(news2.getSearchNum()),Integer.parseInt(news1.getSearchNum() ));
        }
    }
}
