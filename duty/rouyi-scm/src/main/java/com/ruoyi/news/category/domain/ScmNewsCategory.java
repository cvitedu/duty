package com.ruoyi.news.category.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分类名称对象 scm_news_category
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
public class ScmNewsCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 分类 */
    @Excel(name = "分类")
    private String category;


    private Long categoryNum;

    public Long getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(Long categoryNum) {
        this.categoryNum = categoryNum;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("category", getCategory())
            .toString();
    }
}
