package com.ruoyi.news.category.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.news.category.mapper.ScmNewsCategoryMapper;
import com.ruoyi.news.category.domain.ScmNewsCategory;
import com.ruoyi.news.category.service.IScmNewsCategoryService;

/**
 * 分类名称Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
@Service
public class ScmNewsCategoryServiceImpl implements IScmNewsCategoryService 
{
    @Autowired
    private ScmNewsCategoryMapper scmNewsCategoryMapper;

    /**
     * 查询分类名称
     * 
     * @param id 分类名称主键
     * @return 分类名称
     */
    @Override
    public ScmNewsCategory selectScmNewsCategoryById(Long id)
    {
        return scmNewsCategoryMapper.selectScmNewsCategoryById(id);
    }

    /**
     * 查询分类名称列表
     * 
     * @param scmNewsCategory 分类名称
     * @return 分类名称
     */
    @Override
    public List<ScmNewsCategory> selectScmNewsCategoryList(ScmNewsCategory scmNewsCategory)
    {
        return scmNewsCategoryMapper.selectScmNewsCategoryList(scmNewsCategory);
    }

    /**
     * 新增分类名称
     * 
     * @param scmNewsCategory 分类名称
     * @return 结果
     */
    @Override
    public int insertScmNewsCategory(ScmNewsCategory scmNewsCategory)
    {
        return scmNewsCategoryMapper.insertScmNewsCategory(scmNewsCategory);
    }

    /**
     * 修改分类名称
     * 
     * @param scmNewsCategory 分类名称
     * @return 结果
     */
    @Override
    public int updateScmNewsCategory(ScmNewsCategory scmNewsCategory)
    {
        return scmNewsCategoryMapper.updateScmNewsCategory(scmNewsCategory);
    }

    /**
     * 批量删除分类名称
     * 
     * @param ids 需要删除的分类名称主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsCategoryByIds(Long[] ids)
    {
        return scmNewsCategoryMapper.deleteScmNewsCategoryByIds(ids);
    }

    /**
     * 删除分类名称信息
     * 
     * @param id 分类名称主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsCategoryById(Long id)
    {
        return scmNewsCategoryMapper.deleteScmNewsCategoryById(id);
    }
}
