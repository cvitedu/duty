package com.ruoyi.news.category.service;

import java.util.List;
import com.ruoyi.news.category.domain.ScmNewsCategory;

/**
 * 分类名称Service接口
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
public interface IScmNewsCategoryService 
{
    /**
     * 查询分类名称
     * 
     * @param id 分类名称主键
     * @return 分类名称
     */
    public ScmNewsCategory selectScmNewsCategoryById(Long id);

    /**
     * 查询分类名称列表
     * 
     * @param scmNewsCategory 分类名称
     * @return 分类名称集合
     */
    public List<ScmNewsCategory> selectScmNewsCategoryList(ScmNewsCategory scmNewsCategory);

    /**
     * 新增分类名称
     * 
     * @param scmNewsCategory 分类名称
     * @return 结果
     */
    public int insertScmNewsCategory(ScmNewsCategory scmNewsCategory);

    /**
     * 修改分类名称
     * 
     * @param scmNewsCategory 分类名称
     * @return 结果
     */
    public int updateScmNewsCategory(ScmNewsCategory scmNewsCategory);

    /**
     * 批量删除分类名称
     * 
     * @param ids 需要删除的分类名称主键集合
     * @return 结果
     */
    public int deleteScmNewsCategoryByIds(Long[] ids);

    /**
     * 删除分类名称信息
     * 
     * @param id 分类名称主键
     * @return 结果
     */
    public int deleteScmNewsCategoryById(Long id);
}
