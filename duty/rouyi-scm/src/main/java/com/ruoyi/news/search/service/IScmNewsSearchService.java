package com.ruoyi.news.search.service;

import java.util.List;
import com.ruoyi.news.search.domain.ScmNewsSearch;

/**
 * 搜索单Service接口
 * 
 * @author ruoyi
 * @date 2024-04-21
 */
public interface IScmNewsSearchService 
{
    /**
     * 查询搜索单
     * 
     * @param id 搜索单主键
     * @return 搜索单
     */
    public ScmNewsSearch selectScmNewsSearchById(Long id);

    /**
     * 查询搜索单列表
     * 
     * @param scmNewsSearch 搜索单
     * @return 搜索单集合
     */
    public List<ScmNewsSearch> selectScmNewsSearchList(ScmNewsSearch scmNewsSearch);

    /**
     * 新增搜索单
     * 
     * @param scmNewsSearch 搜索单
     * @return 结果
     */
    public int insertScmNewsSearch(ScmNewsSearch scmNewsSearch);

    /**
     * 修改搜索单
     * 
     * @param scmNewsSearch 搜索单
     * @return 结果
     */
    public int updateScmNewsSearch(ScmNewsSearch scmNewsSearch);

    /**
     * 批量删除搜索单
     * 
     * @param ids 需要删除的搜索单主键集合
     * @return 结果
     */
    public int deleteScmNewsSearchByIds(Long[] ids);

    /**
     * 删除搜索单信息
     * 
     * @param id 搜索单主键
     * @return 结果
     */
    public int deleteScmNewsSearchById(Long id);


    public int getMySearch(ScmNewsSearch scmNewsSearch);
}
