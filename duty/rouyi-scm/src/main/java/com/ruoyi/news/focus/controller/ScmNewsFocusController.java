package com.ruoyi.news.focus.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.news.focus.domain.ScmNewsFocus;
import com.ruoyi.news.focus.service.IScmNewsFocusService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户关注Controller
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/UserFocus/UserFocus")
public class ScmNewsFocusController extends BaseController
{
    @Autowired
    private IScmNewsFocusService scmNewsFocusService;

    /**
     * 查询用户关注列表
     */
    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScmNewsFocus scmNewsFocus)
    {
        startPage();
        List<ScmNewsFocus> list = scmNewsFocusService.selectScmNewsFocusList(scmNewsFocus);
        return getDataTable(list);
    }

    /**
     * 导出用户关注列表
     */
    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:export')")
    @Log(title = "用户关注", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScmNewsFocus scmNewsFocus)
    {
        List<ScmNewsFocus> list = scmNewsFocusService.selectScmNewsFocusList(scmNewsFocus);
        ExcelUtil<ScmNewsFocus> util = new ExcelUtil<ScmNewsFocus>(ScmNewsFocus.class);
        util.exportExcel(response, list, "用户关注数据");
    }

    /**
     * 获取用户关注详细信息
     */
    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(scmNewsFocusService.selectScmNewsFocusById(id));
    }

    /**
     * 新增用户关注
     */
    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:add')")
    @Log(title = "用户关注", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScmNewsFocus scmNewsFocus)
    {
        return toAjax(scmNewsFocusService.insertScmNewsFocus(scmNewsFocus));
    }

    /**
     * 修改用户关注
     */
    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:edit')")
    @Log(title = "用户关注", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScmNewsFocus scmNewsFocus)
    {
        return toAjax(scmNewsFocusService.updateScmNewsFocus(scmNewsFocus));
    }

    /**
     * 删除用户关注
     */
    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:remove')")
    @Log(title = "用户关注", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scmNewsFocusService.deleteScmNewsFocusByIds(ids));
    }


    @PreAuthorize("@ss.hasPermi('UserFocus:UserFocus:remove')")
    @Log(title = "用户关注", businessType = BusinessType.DELETE)
    @DeleteMapping()
    public AjaxResult removeF(@RequestBody ScmNewsFocus scmNewsFocus)
    {
        return toAjax(scmNewsFocusService.deleteScmNewsFocus(scmNewsFocus));
    }
}
