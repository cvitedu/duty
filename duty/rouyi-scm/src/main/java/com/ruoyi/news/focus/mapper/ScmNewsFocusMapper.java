package com.ruoyi.news.focus.mapper;

import java.util.List;
import com.ruoyi.news.focus.domain.ScmNewsFocus;

/**
 * 用户关注Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface ScmNewsFocusMapper 
{
    /**
     * 查询用户关注
     * 
     * @param id 用户关注主键
     * @return 用户关注
     */
    public ScmNewsFocus selectScmNewsFocusById(Long id);

    /**
     * 查询用户关注列表
     * 
     * @param scmNewsFocus 用户关注
     * @return 用户关注集合
     */
    public List<ScmNewsFocus> selectScmNewsFocusList(ScmNewsFocus scmNewsFocus);

    /**
     * 新增用户关注
     * 
     * @param scmNewsFocus 用户关注
     * @return 结果
     */
    public int insertScmNewsFocus(ScmNewsFocus scmNewsFocus);

    /**
     * 修改用户关注
     * 
     * @param scmNewsFocus 用户关注
     * @return 结果
     */
    public int updateScmNewsFocus(ScmNewsFocus scmNewsFocus);

    /**
     * 删除用户关注
     * 
     * @param id 用户关注主键
     * @return 结果
     */
    public int deleteScmNewsFocusById(Long id);

    /**
     * 批量删除用户关注
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScmNewsFocusByIds(Long[] ids);
    public int deleteScmNewsFocus(ScmNewsFocus scmNewsFocus);

}
