package com.ruoyi.news.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 热门新闻对象 scm_news
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
public class ScmNews extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 发布人id */
    @Excel(name = "发布人id")
    private Long userId;

    /** 新闻标题 */
    @Excel(name = "新闻标题")
    private String title;

    /** 新闻内容 */
    @Excel(name = "新闻内容")
    private String content;

    /** 发布时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "发布时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date publishDate;

    /** 点赞数量 */
    @Excel(name = "点赞数量")
    private Long num;

    private Long search;

    private String userName;

    private String categoryId;

    private String categoryName;

    private String camBar;

    //头像
    private String avatar;

    //点赞数量
    private Long greatNum;

    public Long getGreatNum() {
        return greatNum;
    }

    public void setGreatNum(Long greatNum) {
        this.greatNum = greatNum;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCamBar() {
        return camBar;
    }

    public void setCamBar(String camBar) {
        this.camBar = camBar;
    }

    public Long getSearch() {
        return search;
    }

    public void setSearch(Long search) {
        this.search = search;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }


    /** 热门新闻视频与图片资源信息 */
    private List<ScmNewsPv> scmNewsPvList;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setPublishDate(Date publishDate) 
    {
        this.publishDate = publishDate;
    }

    public Date getPublishDate() 
    {
        return publishDate;
    }
    public void setNum(Long num) 
    {
        this.num = num;
    }

    public Long getNum() 
    {
        return num;
    }

    public List<ScmNewsPv> getScmNewsPvList()
    {
        return scmNewsPvList;
    }

    public void setScmNewsPvList(List<ScmNewsPv> scmNewsPvList)
    {
        this.scmNewsPvList = scmNewsPvList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("title", getTitle())
            .append("content", getContent())
            .append("publishDate", getPublishDate())
            .append("num", getNum())
            .append("scmNewsPvList", getScmNewsPvList())
            .toString();
    }
}
