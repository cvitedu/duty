package com.ruoyi.news.search.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 搜索单对象 scm_news_search
 * 
 * @author ruoyi
 * @date 2024-04-21
 */
public class ScmNewsSearch extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 搜索名称 */
    @Excel(name = "搜索名称")
    private String searchName;

    /** 搜索次数 */
    @Excel(name = "搜索次数")
    private String searchNum;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSearchName(String searchName) 
    {
        this.searchName = searchName;
    }

    public String getSearchName() 
    {
        return searchName;
    }
    public void setSearchNum(String searchNum) 
    {
        this.searchNum = searchNum;
    }

    public String getSearchNum() 
    {
        return searchNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("searchName", getSearchName())
            .append("searchNum", getSearchNum())
            .toString();
    }
}
