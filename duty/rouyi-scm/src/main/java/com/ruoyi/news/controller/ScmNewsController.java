package com.ruoyi.news.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.news.category.domain.ScmNewsCategory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.news.domain.ScmNews;
import com.ruoyi.news.service.IScmNewsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 热门新闻Controller
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
@RestController
@RequestMapping("/news/news")
public class ScmNewsController extends BaseController
{
    @Autowired
    private IScmNewsService scmNewsService;

    /**
     * 查询热门新闻列表
     */
    @PreAuthorize("@ss.hasPermi('news:news:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScmNews scmNews)
    {
        startPage();
        List<ScmNews> list = scmNewsService.selectScmNewsList(scmNews);
        return getDataTable(list);
    }

    /**
     * 导出热门新闻列表
     */
    @PreAuthorize("@ss.hasPermi('news:news:export')")
    @Log(title = "热门新闻", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScmNews scmNews)
    {
        List<ScmNews> list = scmNewsService.selectScmNewsList(scmNews);
        ExcelUtil<ScmNews> util = new ExcelUtil<ScmNews>(ScmNews.class);
        util.exportExcel(response, list, "热门新闻数据");
    }

    /**
     * 获取热门新闻详细信息
     */
    @PreAuthorize("@ss.hasPermi('news:news:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(scmNewsService.selectScmNewsById(id));
    }

    /**
     * 新增热门新闻
     */
    @PreAuthorize("@ss.hasPermi('news:news:add')")
    @Log(title = "热门新闻", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScmNews scmNews)
    {
        return toAjax(scmNewsService.insertScmNews(scmNews));
    }

    /**
     * 修改热门新闻
     */
    @PreAuthorize("@ss.hasPermi('news:news:edit')")
    @Log(title = "热门新闻", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScmNews scmNews)
    {
        return toAjax(scmNewsService.updateScmNews(scmNews));
    }

    /**
     * 删除热门新闻
     */
    @PreAuthorize("@ss.hasPermi('news:news:remove')")
    @Log(title = "热门新闻", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scmNewsService.deleteScmNewsByIds(ids));
    }


    @PreAuthorize("@ss.hasPermi('news:news:list')")
    @GetMapping("/hot/category/list")
    public TableDataInfo categoryList(ScmNews scmNews)
    {
        startPage();
        List<ScmNewsCategory> list = scmNewsService.selectScmNewsCategoryList(scmNews);
        return getDataTable(list);
    }



    @PreAuthorize("@ss.hasPermi('news:news:list')")
    @GetMapping("/hot/list")
    public TableDataInfo hotList(ScmNews scmNews)
    {
        startPage();
        List<ScmNews> list = scmNewsService.selectScmNewsList(scmNews);
        return getDataTable(list);
    }

}
