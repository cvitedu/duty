package com.ruoyi.news.search.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.news.search.domain.ScmNewsSearch;
import com.ruoyi.news.search.service.IScmNewsSearchService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 搜索单Controller
 * 
 * @author ruoyi
 * @date 2024-04-21
 */
@RestController
@RequestMapping("/Search/Search")
public class ScmNewsSearchController extends BaseController
{
    @Autowired
    private IScmNewsSearchService scmNewsSearchService;

    /**
     * 查询搜索单列表
     */
    @PreAuthorize("@ss.hasPermi('Search:Search:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScmNewsSearch scmNewsSearch)
    {
        startPage();
        List<ScmNewsSearch> list = scmNewsSearchService.selectScmNewsSearchList(scmNewsSearch);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('Search:Search:list')")
    @GetMapping()
    public AjaxResult getMySearch(ScmNewsSearch scmNewsSearch)
    {
        return AjaxResult.success(scmNewsSearchService.getMySearch(scmNewsSearch));
    }

    /**
     * 导出搜索单列表
     */
    @PreAuthorize("@ss.hasPermi('Search:Search:export')")
    @Log(title = "搜索单", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScmNewsSearch scmNewsSearch)
    {
        List<ScmNewsSearch> list = scmNewsSearchService.selectScmNewsSearchList(scmNewsSearch);
        ExcelUtil<ScmNewsSearch> util = new ExcelUtil<ScmNewsSearch>(ScmNewsSearch.class);
        util.exportExcel(response, list, "搜索单数据");
    }

    /**
     * 获取搜索单详细信息
     */
    @PreAuthorize("@ss.hasPermi('Search:Search:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(scmNewsSearchService.selectScmNewsSearchById(id));
    }

    /**
     * 新增搜索单
     */
    @PreAuthorize("@ss.hasPermi('Search:Search:add')")
    @Log(title = "搜索单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScmNewsSearch scmNewsSearch)
    {
        return toAjax(scmNewsSearchService.insertScmNewsSearch(scmNewsSearch));
    }

    /**
     * 修改搜索单
     */
    @PreAuthorize("@ss.hasPermi('Search:Search:edit')")
    @Log(title = "搜索单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScmNewsSearch scmNewsSearch)
    {
        return toAjax(scmNewsSearchService.updateScmNewsSearch(scmNewsSearch));
    }

    /**
     * 删除搜索单
     */
    @PreAuthorize("@ss.hasPermi('Search:Search:remove')")
    @Log(title = "搜索单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scmNewsSearchService.deleteScmNewsSearchByIds(ids));
    }
}
