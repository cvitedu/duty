package com.ruoyi.news.great.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.news.great.domain.ScmNewsGrtus;
import com.ruoyi.news.great.service.IScmNewsGrtusService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户点赞idController
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/NewsGrtus/NewsGrtus")
public class ScmNewsGrtusController extends BaseController
{
    @Autowired
    private IScmNewsGrtusService scmNewsGrtusService;

    /**
     * 查询用户点赞id列表
     */
    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScmNewsGrtus scmNewsGrtus)
    {
        startPage();
        List<ScmNewsGrtus> list = scmNewsGrtusService.selectScmNewsGrtusList(scmNewsGrtus);
        return getDataTable(list);
    }

    /**
     * 导出用户点赞id列表
     */
    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:export')")
    @Log(title = "用户点赞id", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ScmNewsGrtus scmNewsGrtus)
    {
        List<ScmNewsGrtus> list = scmNewsGrtusService.selectScmNewsGrtusList(scmNewsGrtus);
        ExcelUtil<ScmNewsGrtus> util = new ExcelUtil<ScmNewsGrtus>(ScmNewsGrtus.class);
        util.exportExcel(response, list, "用户点赞id数据");
    }

    /**
     * 获取用户点赞id详细信息
     */
    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(scmNewsGrtusService.selectScmNewsGrtusById(id));
    }

    /**
     * 新增用户点赞id
     */
    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:add')")
    @Log(title = "用户点赞id", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScmNewsGrtus scmNewsGrtus)
    {
        return toAjax(scmNewsGrtusService.insertScmNewsGrtus(scmNewsGrtus));
    }

    /**
     * 修改用户点赞id
     */
    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:edit')")
    @Log(title = "用户点赞id", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScmNewsGrtus scmNewsGrtus)
    {
        return toAjax(scmNewsGrtusService.updateScmNewsGrtus(scmNewsGrtus));
    }

    /**
     * 删除用户点赞id
     */
    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:remove')")
    @Log(title = "用户点赞id", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scmNewsGrtusService.deleteScmNewsGrtusByIds(ids));
    }


    @PreAuthorize("@ss.hasPermi('NewsGrtus:NewsGrtus:remove')")
    @Log(title = "用户点赞id", businessType = BusinessType.DELETE)
    @DeleteMapping()
    public AjaxResult removeNU(@RequestBody ScmNewsGrtus scmNewsGrtus)
    {
        return toAjax(scmNewsGrtusService.deleteScmNewsGrtus(scmNewsGrtus));
    }
}
