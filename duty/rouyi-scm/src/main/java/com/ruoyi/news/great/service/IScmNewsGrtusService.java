package com.ruoyi.news.great.service;

import java.util.List;
import com.ruoyi.news.great.domain.ScmNewsGrtus;

/**
 * 用户点赞idService接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface IScmNewsGrtusService 
{
    /**
     * 查询用户点赞id
     * 
     * @param id 用户点赞id主键
     * @return 用户点赞id
     */
    public ScmNewsGrtus selectScmNewsGrtusById(Long id);

    /**
     * 查询用户点赞id列表
     * 
     * @param scmNewsGrtus 用户点赞id
     * @return 用户点赞id集合
     */
    public List<ScmNewsGrtus> selectScmNewsGrtusList(ScmNewsGrtus scmNewsGrtus);

    /**
     * 新增用户点赞id
     * 
     * @param scmNewsGrtus 用户点赞id
     * @return 结果
     */
    public int insertScmNewsGrtus(ScmNewsGrtus scmNewsGrtus);

    /**
     * 修改用户点赞id
     * 
     * @param scmNewsGrtus 用户点赞id
     * @return 结果
     */
    public int updateScmNewsGrtus(ScmNewsGrtus scmNewsGrtus);

    /**
     * 批量删除用户点赞id
     * 
     * @param ids 需要删除的用户点赞id主键集合
     * @return 结果
     */
    public int deleteScmNewsGrtusByIds(Long[] ids);

    /**
     * 删除用户点赞id信息
     * 
     * @param id 用户点赞id主键
     * @return 结果
     */
    public int deleteScmNewsGrtusById(Long id);


    //设置用户点赞数量
    public Long selectCountGreat(Long newId);


    public int deleteScmNewsGrtus( ScmNewsGrtus scmNewsGrtus);

}
