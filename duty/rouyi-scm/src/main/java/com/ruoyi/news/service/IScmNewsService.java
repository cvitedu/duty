package com.ruoyi.news.service;

import java.util.List;

import com.ruoyi.news.category.domain.ScmNewsCategory;
import com.ruoyi.news.domain.ScmNews;

/**
 * 热门新闻Service接口
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
public interface IScmNewsService 
{
    /**
     * 查询热门新闻
     * 
     * @param id 热门新闻主键
     * @return 热门新闻
     */
    public ScmNews selectScmNewsById(Long id);

    /**
     * 查询热门新闻列表
     * 
     * @param scmNews 热门新闻
     * @return 热门新闻集合
     */
    public List<ScmNews> selectScmNewsList(ScmNews scmNews);

    /**
     * 新增热门新闻
     * 
     * @param scmNews 热门新闻
     * @return 结果
     */
    public int insertScmNews(ScmNews scmNews);

    /**
     * 修改热门新闻
     * 
     * @param scmNews 热门新闻
     * @return 结果
     */
    public int updateScmNews(ScmNews scmNews);

    /**
     * 批量删除热门新闻
     * 
     * @param ids 需要删除的热门新闻主键集合
     * @return 结果
     */
    public int deleteScmNewsByIds(Long[] ids);

    /**
     * 删除热门新闻信息
     * 
     * @param id 热门新闻主键
     * @return 结果
     */
    public int deleteScmNewsById(Long id);

    public List<ScmNewsCategory> selectScmNewsCategoryList(ScmNews scmNews);

    public List<ScmNews> selectScmNewsHotList(ScmNews scmNews);

}
