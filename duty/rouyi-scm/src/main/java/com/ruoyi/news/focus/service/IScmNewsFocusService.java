package com.ruoyi.news.focus.service;

import java.util.List;
import com.ruoyi.news.focus.domain.ScmNewsFocus;

/**
 * 用户关注Service接口
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
public interface IScmNewsFocusService 
{
    /**
     * 查询用户关注
     * 
     * @param id 用户关注主键
     * @return 用户关注
     */
    public ScmNewsFocus selectScmNewsFocusById(Long id);

    /**
     * 查询用户关注列表
     * 
     * @param scmNewsFocus 用户关注
     * @return 用户关注集合
     */
    public List<ScmNewsFocus> selectScmNewsFocusList(ScmNewsFocus scmNewsFocus);

    /**
     * 新增用户关注
     * 
     * @param scmNewsFocus 用户关注
     * @return 结果
     */
    public int insertScmNewsFocus(ScmNewsFocus scmNewsFocus);

    /**
     * 修改用户关注
     * 
     * @param scmNewsFocus 用户关注
     * @return 结果
     */
    public int updateScmNewsFocus(ScmNewsFocus scmNewsFocus);

    /**
     * 批量删除用户关注
     * 
     * @param ids 需要删除的用户关注主键集合
     * @return 结果
     */
    public int deleteScmNewsFocusByIds(Long[] ids);

    /**
     * 删除用户关注信息
     * 
     * @param id 用户关注主键
     * @return 结果
     */
    public int deleteScmNewsFocusById(Long id);
    public int deleteScmNewsFocus(ScmNewsFocus scmNewsFocus);

}
