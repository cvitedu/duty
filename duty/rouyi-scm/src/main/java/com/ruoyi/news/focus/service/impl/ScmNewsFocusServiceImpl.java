package com.ruoyi.news.focus.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.news.focus.mapper.ScmNewsFocusMapper;
import com.ruoyi.news.focus.domain.ScmNewsFocus;
import com.ruoyi.news.focus.service.IScmNewsFocusService;

/**
 * 用户关注Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-25
 */
@Service
public class ScmNewsFocusServiceImpl implements IScmNewsFocusService 
{
    @Autowired
    private ScmNewsFocusMapper scmNewsFocusMapper;

    /**
     * 查询用户关注
     * 
     * @param id 用户关注主键
     * @return 用户关注
     */
    @Override
    public ScmNewsFocus selectScmNewsFocusById(Long id)
    {
        return scmNewsFocusMapper.selectScmNewsFocusById(id);
    }

    /**
     * 查询用户关注列表
     * 
     * @param scmNewsFocus 用户关注
     * @return 用户关注
     */
    @Override
    public List<ScmNewsFocus> selectScmNewsFocusList(ScmNewsFocus scmNewsFocus)
    {
        return scmNewsFocusMapper.selectScmNewsFocusList(scmNewsFocus);
    }

    /**
     * 新增用户关注
     * 
     * @param scmNewsFocus 用户关注
     * @return 结果
     */
    @Override
    public int insertScmNewsFocus(ScmNewsFocus scmNewsFocus)
    {
        return scmNewsFocusMapper.insertScmNewsFocus(scmNewsFocus);
    }

    /**
     * 修改用户关注
     * 
     * @param scmNewsFocus 用户关注
     * @return 结果
     */
    @Override
    public int updateScmNewsFocus(ScmNewsFocus scmNewsFocus)
    {
        return scmNewsFocusMapper.updateScmNewsFocus(scmNewsFocus);
    }

    /**
     * 批量删除用户关注
     * 
     * @param ids 需要删除的用户关注主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsFocusByIds(Long[] ids)
    {
        return scmNewsFocusMapper.deleteScmNewsFocusByIds(ids);
    }

    /**
     * 删除用户关注信息
     * 
     * @param id 用户关注主键
     * @return 结果
     */
    @Override
    public int deleteScmNewsFocusById(Long id)
    {
        return scmNewsFocusMapper.deleteScmNewsFocusById(id);
    }

    @Override
    public int deleteScmNewsFocus(ScmNewsFocus scmNewsFocus) {
        return scmNewsFocusMapper.deleteScmNewsFocus(scmNewsFocus);
    }
}
