package com.ruoyi.news.mapper;

import java.util.List;
import com.ruoyi.news.domain.ScmNews;
import com.ruoyi.news.domain.ScmNewsPv;

/**
 * 热门新闻Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
public interface ScmNewsMapper 
{
    /**
     * 查询热门新闻
     * 
     * @param id 热门新闻主键
     * @return 热门新闻
     */
    public ScmNews selectScmNewsById(Long id);

    /**
     * 查询热门新闻列表
     * 
     * @param scmNews 热门新闻
     * @return 热门新闻集合
     */
    public List<ScmNews> selectScmNewsList(ScmNews scmNews);

    /**
     * 新增热门新闻
     * 
     * @param scmNews 热门新闻
     * @return 结果
     */
    public int insertScmNews(ScmNews scmNews);

    /**
     * 修改热门新闻
     * 
     * @param scmNews 热门新闻
     * @return 结果
     */
    public int updateScmNews(ScmNews scmNews);

    /**
     * 删除热门新闻
     * 
     * @param id 热门新闻主键
     * @return 结果
     */
    public int deleteScmNewsById(Long id);

    /**
     * 批量删除热门新闻
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScmNewsByIds(Long[] ids);

    /**
     * 批量删除热门新闻视频与图片资源
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteScmNewsPvByNewsIds(Long[] ids);
    
    /**
     * 批量新增热门新闻视频与图片资源
     * 
     * @param scmNewsPvList 热门新闻视频与图片资源列表
     * @return 结果
     */
    public int batchScmNewsPv(List<ScmNewsPv> scmNewsPvList);
    

    /**
     * 通过热门新闻主键删除热门新闻视频与图片资源信息
     * 
     * @param id 热门新闻ID
     * @return 结果
     */
    public int deleteScmNewsPvByNewsId(Long id);
}
