package com.ruoyi.news.service.impl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.ruoyi.news.category.domain.ScmNewsCategory;
import com.ruoyi.news.category.service.IScmNewsCategoryService;
import com.ruoyi.news.great.service.IScmNewsGrtusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.news.domain.ScmNewsPv;
import com.ruoyi.news.mapper.ScmNewsMapper;
import com.ruoyi.news.domain.ScmNews;
import com.ruoyi.news.service.IScmNewsService;

/**
 * 热门新闻Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-18
 */
@Service
public class ScmNewsServiceImpl implements IScmNewsService 
{
    @Autowired
    private ScmNewsMapper scmNewsMapper;

    @Autowired
    private IScmNewsCategoryService scmNewsCategoryService;

    @Autowired
    private IScmNewsGrtusService greatService;

    /**
     * 查询热门新闻
     * 
     * @param id 热门新闻主键
     * @return 热门新闻
     */
    @Override
    public ScmNews selectScmNewsById(Long id)
    {
        return scmNewsMapper.selectScmNewsById(id);
    }

    /**
     * 查询热门新闻列表
     * 
     * @param scmNews 热门新闻
     * @return 热门新闻
     */
    @Override
    public List<ScmNews>  selectScmNewsList(ScmNews scmNews)
    {
        List<ScmNews> list = scmNewsMapper.selectScmNewsList(scmNews);
        for (ScmNews news : list) {
            news.setGreatNum(greatService.selectCountGreat(news.getId()));
        }
        return list;
    }

    /**
     * 新增热门新闻
     * 
     * @param scmNews 热门新闻
     * @return 结果
     */
    @Transactional
    @Override
    public int insertScmNews(ScmNews scmNews)
    {
        int rows = scmNewsMapper.insertScmNews(scmNews);
        insertScmNewsPv(scmNews);
        return rows;
    }

    /**
     * 修改热门新闻
     * 
     * @param scmNews 热门新闻
     * @return 结果
     */
    @Transactional
    @Override
    public int updateScmNews(ScmNews scmNews)
    {
        scmNewsMapper.deleteScmNewsPvByNewsId(scmNews.getId());
        insertScmNewsPv(scmNews);
        return scmNewsMapper.updateScmNews(scmNews);
    }

    /**
     * 批量删除热门新闻
     * 
     * @param ids 需要删除的热门新闻主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteScmNewsByIds(Long[] ids)
    {
        scmNewsMapper.deleteScmNewsPvByNewsIds(ids);
        return scmNewsMapper.deleteScmNewsByIds(ids);
    }

    /**
     * 删除热门新闻信息
     * 
     * @param id 热门新闻主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteScmNewsById(Long id)
    {
        scmNewsMapper.deleteScmNewsPvByNewsId(id);
        return scmNewsMapper.deleteScmNewsById(id);
    }

    @Override
    public List<ScmNewsCategory> selectScmNewsCategoryList(ScmNews scmNews) {

        List<ScmNews> list = scmNewsMapper.selectScmNewsList(scmNews);
        List<ScmNewsCategory> categories = scmNewsCategoryService.selectScmNewsCategoryList(new ScmNewsCategory());
        for (ScmNews scm : list) {
            for (ScmNewsCategory category : categories) {
                if (category.getCategoryNum() == null){
                    category.setCategoryNum(0L);
                }
                if (scm.getNum() == null){
                    scm.setNum(0L);
                }
                if (category.getId().equals(Long.valueOf( scm.getCategoryId()))){

                    category.setCategoryNum(category.getCategoryNum()+scm.getNum());
                }
            }
        }

        Collections.sort(categories, new ScmNewsComparator());
        return categories ;
    }

    @Override
    public List<ScmNews> selectScmNewsHotList(ScmNews scmNews) {
        List<ScmNews> list = scmNewsMapper.selectScmNewsList(scmNews);
        Collections.sort(list , new ScmNewsHotComparator());
        return list;
    }

    /**
     * 新增热门新闻视频与图片资源信息
     * 
     * @param scmNews 热门新闻对象
     */
    public void insertScmNewsPv(ScmNews scmNews)
    {
        List<ScmNewsPv> scmNewsPvList = scmNews.getScmNewsPvList();
        Long id = scmNews.getId();
        if (StringUtils.isNotNull(scmNewsPvList))
        {
            List<ScmNewsPv> list = new ArrayList<ScmNewsPv>();
            for (ScmNewsPv scmNewsPv : scmNewsPvList)
            {
                scmNewsPv.setNewsId(id);
                list.add(scmNewsPv);
            }
            if (list.size() > 0)
            {
                scmNewsMapper.batchScmNewsPv(list);
            }
        }
    }


    //比较方法
    private class ScmNewsComparator implements Comparator<ScmNewsCategory> {
        @Override
        public int compare(ScmNewsCategory news1, ScmNewsCategory news2) {
            return Long.compare(news2.getCategoryNum(),news1.getCategoryNum() );
        }
    }


    private class ScmNewsHotComparator implements Comparator<ScmNews> {
        @Override
        public int compare(ScmNews news1, ScmNews news2) {
            return Long.compare(news2.getNum(),news1.getNum() );
        }
    }
}
