package com.ruoyi.zrsfl.service;

import java.util.List;
import com.ruoyi.zrsfl.domain.zrsfl;

/**
 * 值日生分类Service接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface IzrsflService 
{
    /**
     * 查询值日生分类
     * 
     * @param id 值日生分类主键
     * @return 值日生分类
     */
    public zrsfl selectzrsflById(Long id);

    /**
     * 查询值日生分类列表
     * 
     * @param zrsfl 值日生分类
     * @return 值日生分类集合
     */
    public List<zrsfl> selectzrsflList(zrsfl zrsfl);

    /**
     * 新增值日生分类
     * 
     * @param zrsfl 值日生分类
     * @return 结果
     */
    public int insertzrsfl(zrsfl zrsfl);

    /**
     * 修改值日生分类
     * 
     * @param zrsfl 值日生分类
     * @return 结果
     */
    public int updatezrsfl(zrsfl zrsfl);

    /**
     * 批量删除值日生分类
     * 
     * @param ids 需要删除的值日生分类主键集合
     * @return 结果
     */
    public int deletezrsflByIds(Long[] ids);

    /**
     * 删除值日生分类信息
     * 
     * @param id 值日生分类主键
     * @return 结果
     */
    public int deletezrsflById(Long id);
}
