package com.ruoyi.zrsfl.mapper;

import java.util.List;
import com.ruoyi.zrsfl.domain.zrsfl;

/**
 * 值日生分类Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
public interface zrsflMapper 
{
    /**
     * 查询值日生分类
     * 
     * @param id 值日生分类主键
     * @return 值日生分类
     */
    public zrsfl selectzrsflById(Long id);

    /**
     * 查询值日生分类列表
     * 
     * @param zrsfl 值日生分类
     * @return 值日生分类集合
     */
    public List<zrsfl> selectzrsflList(zrsfl zrsfl);

    /**
     * 新增值日生分类
     * 
     * @param zrsfl 值日生分类
     * @return 结果
     */
    public int insertzrsfl(zrsfl zrsfl);

    /**
     * 修改值日生分类
     * 
     * @param zrsfl 值日生分类
     * @return 结果
     */
    public int updatezrsfl(zrsfl zrsfl);

    /**
     * 删除值日生分类
     * 
     * @param id 值日生分类主键
     * @return 结果
     */
    public int deletezrsflById(Long id);

    /**
     * 批量删除值日生分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletezrsflByIds(Long[] ids);
}
