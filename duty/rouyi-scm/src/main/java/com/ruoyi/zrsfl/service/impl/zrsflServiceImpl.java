package com.ruoyi.zrsfl.service.impl;

import java.util.List;

import com.ruoyi.zrsfl.domain.zrsfl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.zrsfl.service.IzrsflService;

/**
 * 值日生分类Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-16
 */
@Service
public class zrsflServiceImpl implements IzrsflService 
{
    @Autowired
    private com.ruoyi.zrsfl.mapper.zrsflMapper zrsflMapper;

    /**
     * 查询值日生分类
     * 
     * @param id 值日生分类主键
     * @return 值日生分类
     */
    @Override
    public zrsfl selectzrsflById(Long id)
    {
        return zrsflMapper.selectzrsflById(id);
    }

    /**
     * 查询值日生分类列表
     * 
     * @param zrsfl 值日生分类
     * @return 值日生分类
     */
    @Override
    public List<zrsfl> selectzrsflList(zrsfl zrsfl)
    {
        return zrsflMapper.selectzrsflList(zrsfl);
    }

    /**
     * 新增值日生分类
     * 
     * @param zrsfl 值日生分类
     * @return 结果
     */
    @Override
    public int insertzrsfl(zrsfl zrsfl)
    {
        return zrsflMapper.insertzrsfl(zrsfl);
    }

    /**
     * 修改值日生分类
     * 
     * @param zrsfl 值日生分类
     * @return 结果
     */
    @Override
    public int updatezrsfl(zrsfl zrsfl)
    {
        return zrsflMapper.updatezrsfl(zrsfl);
    }

    /**
     * 批量删除值日生分类
     * 
     * @param ids 需要删除的值日生分类主键
     * @return 结果
     */
    @Override
    public int deletezrsflByIds(Long[] ids)
    {
        return zrsflMapper.deletezrsflByIds(ids);
    }

    /**
     * 删除值日生分类信息
     * 
     * @param id 值日生分类主键
     * @return 结果
     */
    @Override
    public int deletezrsflById(Long id)
    {
        return zrsflMapper.deletezrsflById(id);
    }
}
