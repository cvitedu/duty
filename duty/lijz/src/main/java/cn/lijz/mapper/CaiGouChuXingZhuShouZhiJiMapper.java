package cn.lijz.mapper;

import java.util.List;
import cn.lijz.domain.CaiGouChuXingZhuShouZhiJi;

/**
 * 采购出行助手值机Mapper接口
 * 
 * @author lijz
 * @date 2024-04-25
 */
public interface CaiGouChuXingZhuShouZhiJiMapper 
{
    /**
     * 查询采购出行助手值机
     * 
     * @param id 采购出行助手值机主键
     * @return 采购出行助手值机
     */
    public CaiGouChuXingZhuShouZhiJi selectCaiGouChuXingZhuShouZhiJiById(Long id);

    /**
     * 查询采购出行助手值机列表
     * 
     * @param caiGouChuXingZhuShouZhiJi 采购出行助手值机
     * @return 采购出行助手值机集合
     */
    public List<CaiGouChuXingZhuShouZhiJi> selectCaiGouChuXingZhuShouZhiJiList(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi);

    /**
     * 新增采购出行助手值机
     * 
     * @param caiGouChuXingZhuShouZhiJi 采购出行助手值机
     * @return 结果
     */
    public int insertCaiGouChuXingZhuShouZhiJi(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi);

    /**
     * 修改采购出行助手值机
     * 
     * @param caiGouChuXingZhuShouZhiJi 采购出行助手值机
     * @return 结果
     */
    public int updateCaiGouChuXingZhuShouZhiJi(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi);

    /**
     * 删除采购出行助手值机
     * 
     * @param id 采购出行助手值机主键
     * @return 结果
     */
    public int deleteCaiGouChuXingZhuShouZhiJiById(Long id);

    /**
     * 批量删除采购出行助手值机
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteCaiGouChuXingZhuShouZhiJiByIds(Long[] ids);
}
