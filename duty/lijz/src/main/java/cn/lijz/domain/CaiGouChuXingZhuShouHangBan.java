package cn.lijz.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购出行助手航班对象 cai_gou_chu_xing_zhu_shou_hang_ban
 * 
 * @author lijz
 * @date 2024-04-25
 */
public class CaiGouChuXingZhuShouHangBan extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 航班号 */
    @Excel(name = "航班号")
    private String hbh;

    /** 机型 */
    @Excel(name = "机型")
    private String jx;

    /** 仓位 */
    @Excel(name = "仓位")
    private String cw;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setHbh(String hbh) 
    {
        this.hbh = hbh;
    }

    public String getHbh() 
    {
        return hbh;
    }
    public void setJx(String jx) 
    {
        this.jx = jx;
    }

    public String getJx() 
    {
        return jx;
    }
    public void setCw(String cw) 
    {
        this.cw = cw;
    }

    public String getCw() 
    {
        return cw;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hbh", getHbh())
            .append("jx", getJx())
            .append("cw", getCw())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
