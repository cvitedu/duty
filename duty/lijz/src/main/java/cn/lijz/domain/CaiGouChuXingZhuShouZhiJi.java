package cn.lijz.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 采购出行助手值机对象 cai_gou_chu_xing_zhu_shou_zhi_ji
 * 
 * @author lijz
 * @date 2024-04-25
 */
@Data
public class CaiGouChuXingZhuShouZhiJi extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 助手ID */
    private Long zhuShouId;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 航班ID */
    @Excel(name = "航班ID")
    private Long hbId;

    /** 座位 */
    @Excel(name = "座位")
    private String zw;
}
