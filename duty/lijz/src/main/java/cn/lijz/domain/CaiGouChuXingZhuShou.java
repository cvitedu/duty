package cn.lijz.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 采购出行助手对象 cai_gou_chu_xing_zhu_shou
 * 
 * @author lijz
 * @date 2024-04-21
 */
@Data
public class CaiGouChuXingZhuShou extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 用户ID */
    private Long yhId;

    /** 航班ID */
    private Long hbId;

    /** 航班号 */
    @Excel(name = "航班号")
    private String hbh;

    /** 机型 */
    @Excel(name = "机型")
    private String jx;

    /** 航空公司LOGO */
    @Excel(name = "航空公司LOGO")
    private String logo;

    /** 行程日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "行程日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date xcrq;

    /** 出发时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "出发时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cfsj;

    /** 出发城市 */
    @Excel(name = "出发城市")
    private String cfcs;

    /** 到达时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "到达时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ddsj;

    /** 到达城市 */
    @Excel(name = "到达城市")
    private String ddcs;

    /** 行程状态 */
    @Excel(name = "行程状态")
    private String xczt;

    /** 行程状态取反查询 */
    private String nxczt;
}
