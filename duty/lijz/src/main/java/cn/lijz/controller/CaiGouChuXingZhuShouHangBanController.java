package cn.lijz.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import cn.lijz.domain.CaiGouChuXingZhuShouHangBan;
import cn.lijz.service.ICaiGouChuXingZhuShouHangBanService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购出行助手航班Controller
 * 
 * @author lijz
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/lijz/CaiGouChuXingZhuShouHangBan")
public class CaiGouChuXingZhuShouHangBanController extends BaseController
{
    @Autowired
    private ICaiGouChuXingZhuShouHangBanService caiGouChuXingZhuShouHangBanService;

    /**
     * 查询采购出行助手航班列表
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouHangBan:list')")
    @GetMapping("/list")
    public TableDataInfo list(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        startPage();
        List<CaiGouChuXingZhuShouHangBan> list = caiGouChuXingZhuShouHangBanService.selectCaiGouChuXingZhuShouHangBanList(caiGouChuXingZhuShouHangBan);
        return getDataTable(list);
    }

    /**
     * 导出采购出行助手航班列表
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouHangBan:export')")
    @Log(title = "采购出行助手航班", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        List<CaiGouChuXingZhuShouHangBan> list = caiGouChuXingZhuShouHangBanService.selectCaiGouChuXingZhuShouHangBanList(caiGouChuXingZhuShouHangBan);
        ExcelUtil<CaiGouChuXingZhuShouHangBan> util = new ExcelUtil<CaiGouChuXingZhuShouHangBan>(CaiGouChuXingZhuShouHangBan.class);
        util.exportExcel(response, list, "采购出行助手航班数据");
    }

    /**
     * 获取采购出行助手航班详细信息
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouHangBan:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(caiGouChuXingZhuShouHangBanService.selectCaiGouChuXingZhuShouHangBanById(id));
    }

    /**
     * 新增采购出行助手航班
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouHangBan:add')")
    @Log(title = "采购出行助手航班", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        return toAjax(caiGouChuXingZhuShouHangBanService.insertCaiGouChuXingZhuShouHangBan(caiGouChuXingZhuShouHangBan));
    }

    /**
     * 修改采购出行助手航班
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouHangBan:edit')")
    @Log(title = "采购出行助手航班", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        return toAjax(caiGouChuXingZhuShouHangBanService.updateCaiGouChuXingZhuShouHangBan(caiGouChuXingZhuShouHangBan));
    }

    /**
     * 删除采购出行助手航班
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouHangBan:remove')")
    @Log(title = "采购出行助手航班", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(caiGouChuXingZhuShouHangBanService.deleteCaiGouChuXingZhuShouHangBanByIds(ids));
    }
}
