package cn.lijz.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import cn.lijz.domain.CaiGouChuXingZhuShouZhiJi;
import cn.lijz.service.ICaiGouChuXingZhuShouZhiJiService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购出行助手值机Controller
 * 
 * @author lijz
 * @date 2024-04-25
 */
@RestController
@RequestMapping("/lijz/CaiGouChuXingZhuShouZhiJi")
public class CaiGouChuXingZhuShouZhiJiController extends BaseController
{
    @Autowired
    private ICaiGouChuXingZhuShouZhiJiService caiGouChuXingZhuShouZhiJiService;

    /**
     * 查询采购出行助手值机列表
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouZhiJi:list')")
    @GetMapping("/list")
    public TableDataInfo list(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        startPage();
        List<CaiGouChuXingZhuShouZhiJi> list = caiGouChuXingZhuShouZhiJiService.selectCaiGouChuXingZhuShouZhiJiList(caiGouChuXingZhuShouZhiJi);
        return getDataTable(list);
    }

    /**
     * 导出采购出行助手值机列表
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouZhiJi:export')")
    @Log(title = "采购出行助手值机", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        List<CaiGouChuXingZhuShouZhiJi> list = caiGouChuXingZhuShouZhiJiService.selectCaiGouChuXingZhuShouZhiJiList(caiGouChuXingZhuShouZhiJi);
        ExcelUtil<CaiGouChuXingZhuShouZhiJi> util = new ExcelUtil<CaiGouChuXingZhuShouZhiJi>(CaiGouChuXingZhuShouZhiJi.class);
        util.exportExcel(response, list, "采购出行助手值机数据");
    }

    /**
     * 获取采购出行助手值机详细信息
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouZhiJi:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(caiGouChuXingZhuShouZhiJiService.selectCaiGouChuXingZhuShouZhiJiById(id));
    }

    /**
     * 新增采购出行助手值机
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouZhiJi:add')")
    @Log(title = "采购出行助手值机", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        return toAjax(caiGouChuXingZhuShouZhiJiService.insertCaiGouChuXingZhuShouZhiJi(caiGouChuXingZhuShouZhiJi));
    }

    /**
     * 修改采购出行助手值机
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouZhiJi:edit')")
    @Log(title = "采购出行助手值机", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        return toAjax(caiGouChuXingZhuShouZhiJiService.updateCaiGouChuXingZhuShouZhiJi(caiGouChuXingZhuShouZhiJi));
    }

    /**
     * 删除采购出行助手值机
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShouZhiJi:remove')")
    @Log(title = "采购出行助手值机", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(caiGouChuXingZhuShouZhiJiService.deleteCaiGouChuXingZhuShouZhiJiByIds(ids));
    }
}
