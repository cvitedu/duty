package cn.lijz.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import cn.lijz.domain.CaiGouChuXingZhuShou;
import cn.lijz.service.ICaiGouChuXingZhuShouService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 采购出行助手Controller
 * 
 * @author lijz
 * @date 2024-04-21
 */
@RestController
@RequestMapping("/lijz/CaiGouChuXingZhuShou")
public class CaiGouChuXingZhuShouController extends BaseController
{
    @Autowired
    private ICaiGouChuXingZhuShouService caiGouChuXingZhuShouService;

    /**
     * 查询采购出行助手列表
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShou:list')")
    @GetMapping("/list")
    public TableDataInfo list(CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        startPage();
        List<CaiGouChuXingZhuShou> list = caiGouChuXingZhuShouService.selectCaiGouChuXingZhuShouList(caiGouChuXingZhuShou);
        return getDataTable(list);
    }

    /**
     * 导出采购出行助手列表
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShou:export')")
    @Log(title = "采购出行助手", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        List<CaiGouChuXingZhuShou> list = caiGouChuXingZhuShouService.selectCaiGouChuXingZhuShouList(caiGouChuXingZhuShou);
        ExcelUtil<CaiGouChuXingZhuShou> util = new ExcelUtil<CaiGouChuXingZhuShou>(CaiGouChuXingZhuShou.class);
        util.exportExcel(response, list, "采购出行助手数据");
    }

    /**
     * 获取采购出行助手详细信息
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShou:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(caiGouChuXingZhuShouService.selectCaiGouChuXingZhuShouById(id));
    }

    /**
     * 新增采购出行助手
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShou:add')")
    @Log(title = "采购出行助手", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        caiGouChuXingZhuShou.setYhId(SecurityUtils.getUserId());
        return toAjax(caiGouChuXingZhuShouService.insertCaiGouChuXingZhuShou(caiGouChuXingZhuShou));
    }

    /**
     * 修改采购出行助手
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShou:edit')")
    @Log(title = "采购出行助手", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        return toAjax(caiGouChuXingZhuShouService.updateCaiGouChuXingZhuShou(caiGouChuXingZhuShou));
    }

    /**
     * 删除采购出行助手
     */
    @PreAuthorize("@ss.hasPermi('lijz:CaiGouChuXingZhuShou:remove')")
    @Log(title = "采购出行助手", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(caiGouChuXingZhuShouService.deleteCaiGouChuXingZhuShouByIds(ids));
    }
}
