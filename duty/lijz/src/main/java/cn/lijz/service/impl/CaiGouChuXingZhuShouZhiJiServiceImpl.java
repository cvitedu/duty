package cn.lijz.service.impl;

import java.util.List;

import cn.lijz.domain.CaiGouChuXingZhuShou;
import cn.lijz.mapper.CaiGouChuXingZhuShouMapper;
import com.ruoyi.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.lijz.mapper.CaiGouChuXingZhuShouZhiJiMapper;
import cn.lijz.domain.CaiGouChuXingZhuShouZhiJi;
import cn.lijz.service.ICaiGouChuXingZhuShouZhiJiService;

/**
 * 采购出行助手值机Service业务层处理
 * 
 * @author lijz
 * @date 2024-04-25
 */
@Service
public class CaiGouChuXingZhuShouZhiJiServiceImpl implements ICaiGouChuXingZhuShouZhiJiService 
{
    private static final Logger log = LoggerFactory.getLogger(CaiGouChuXingZhuShouZhiJiServiceImpl.class);
    @Autowired
    private CaiGouChuXingZhuShouZhiJiMapper caiGouChuXingZhuShouZhiJiMapper;
    @Autowired
    private CaiGouChuXingZhuShouMapper caiGouChuXingZhuShouMapper;

    /**
     * 查询采购出行助手值机
     * 
     * @param id 采购出行助手值机主键
     * @return 采购出行助手值机
     */
    @Override
    public CaiGouChuXingZhuShouZhiJi selectCaiGouChuXingZhuShouZhiJiById(Long id)
    {
        return caiGouChuXingZhuShouZhiJiMapper.selectCaiGouChuXingZhuShouZhiJiById(id);
    }

    /**
     * 查询采购出行助手值机列表
     * 
     * @param caiGouChuXingZhuShouZhiJi 采购出行助手值机
     * @return 采购出行助手值机
     */
    @Override
    public List<CaiGouChuXingZhuShouZhiJi> selectCaiGouChuXingZhuShouZhiJiList(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        return caiGouChuXingZhuShouZhiJiMapper.selectCaiGouChuXingZhuShouZhiJiList(caiGouChuXingZhuShouZhiJi);
    }

    /**
     * 新增采购出行助手值机
     * 
     * @param caiGouChuXingZhuShouZhiJi 采购出行助手值机
     * @return 结果
     */
    @Override
    public int insertCaiGouChuXingZhuShouZhiJi(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        caiGouChuXingZhuShouZhiJi.setCreateTime(DateUtils.getNowDate());
        int results = caiGouChuXingZhuShouZhiJiMapper.insertCaiGouChuXingZhuShouZhiJi(caiGouChuXingZhuShouZhiJi);
        if (results > 0) {
            CaiGouChuXingZhuShou caiGouChuXingZhuShou = new CaiGouChuXingZhuShou();
            caiGouChuXingZhuShou.setId(caiGouChuXingZhuShouZhiJi.getZhuShouId());
            caiGouChuXingZhuShou.setYhId(caiGouChuXingZhuShouZhiJi.getUserId());
            caiGouChuXingZhuShou.setHbId(caiGouChuXingZhuShouZhiJi.getHbId());
            caiGouChuXingZhuShou.setXczt("1");
            int i = caiGouChuXingZhuShouMapper.updateCaiGouChuXingZhuShou(caiGouChuXingZhuShou);
            log.debug(String.valueOf(i));
        }
        return results;
    }

    /**
     * 修改采购出行助手值机
     * 
     * @param caiGouChuXingZhuShouZhiJi 采购出行助手值机
     * @return 结果
     */
    @Override
    public int updateCaiGouChuXingZhuShouZhiJi(CaiGouChuXingZhuShouZhiJi caiGouChuXingZhuShouZhiJi)
    {
        caiGouChuXingZhuShouZhiJi.setUpdateTime(DateUtils.getNowDate());
        return caiGouChuXingZhuShouZhiJiMapper.updateCaiGouChuXingZhuShouZhiJi(caiGouChuXingZhuShouZhiJi);
    }

    /**
     * 批量删除采购出行助手值机
     * 
     * @param ids 需要删除的采购出行助手值机主键
     * @return 结果
     */
    @Override
    public int deleteCaiGouChuXingZhuShouZhiJiByIds(Long[] ids)
    {
        return caiGouChuXingZhuShouZhiJiMapper.deleteCaiGouChuXingZhuShouZhiJiByIds(ids);
    }

    /**
     * 删除采购出行助手值机信息
     * 
     * @param id 采购出行助手值机主键
     * @return 结果
     */
    @Override
    public int deleteCaiGouChuXingZhuShouZhiJiById(Long id)
    {
        return caiGouChuXingZhuShouZhiJiMapper.deleteCaiGouChuXingZhuShouZhiJiById(id);
    }
}
