package cn.lijz.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.lijz.mapper.CaiGouChuXingZhuShouMapper;
import cn.lijz.domain.CaiGouChuXingZhuShou;
import cn.lijz.service.ICaiGouChuXingZhuShouService;

/**
 * 采购出行助手Service业务层处理
 * 
 * @author lijz
 * @date 2024-04-21
 */
@Service
public class CaiGouChuXingZhuShouServiceImpl implements ICaiGouChuXingZhuShouService 
{
    @Autowired
    private CaiGouChuXingZhuShouMapper caiGouChuXingZhuShouMapper;

    /**
     * 查询采购出行助手
     * 
     * @param id 采购出行助手主键
     * @return 采购出行助手
     */
    @Override
    public CaiGouChuXingZhuShou selectCaiGouChuXingZhuShouById(Long id)
    {
        return caiGouChuXingZhuShouMapper.selectCaiGouChuXingZhuShouById(id);
    }

    /**
     * 查询采购出行助手列表
     * 
     * @param caiGouChuXingZhuShou 采购出行助手
     * @return 采购出行助手
     */
    @Override
    public List<CaiGouChuXingZhuShou> selectCaiGouChuXingZhuShouList(CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        return caiGouChuXingZhuShouMapper.selectCaiGouChuXingZhuShouList(caiGouChuXingZhuShou);
    }

    /**
     * 新增采购出行助手
     * 
     * @param caiGouChuXingZhuShou 采购出行助手
     * @return 结果
     */
    @Override
    public int insertCaiGouChuXingZhuShou(CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        caiGouChuXingZhuShou.setCreateTime(DateUtils.getNowDate());
        return caiGouChuXingZhuShouMapper.insertCaiGouChuXingZhuShou(caiGouChuXingZhuShou);
    }

    /**
     * 修改采购出行助手
     * 
     * @param caiGouChuXingZhuShou 采购出行助手
     * @return 结果
     */
    @Override
    public int updateCaiGouChuXingZhuShou(CaiGouChuXingZhuShou caiGouChuXingZhuShou)
    {
        caiGouChuXingZhuShou.setUpdateTime(DateUtils.getNowDate());
        return caiGouChuXingZhuShouMapper.updateCaiGouChuXingZhuShou(caiGouChuXingZhuShou);
    }

    /**
     * 批量删除采购出行助手
     * 
     * @param ids 需要删除的采购出行助手主键
     * @return 结果
     */
    @Override
    public int deleteCaiGouChuXingZhuShouByIds(Long[] ids)
    {
        return caiGouChuXingZhuShouMapper.deleteCaiGouChuXingZhuShouByIds(ids);
    }

    /**
     * 删除采购出行助手信息
     * 
     * @param id 采购出行助手主键
     * @return 结果
     */
    @Override
    public int deleteCaiGouChuXingZhuShouById(Long id)
    {
        return caiGouChuXingZhuShouMapper.deleteCaiGouChuXingZhuShouById(id);
    }
}
