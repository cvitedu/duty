package cn.lijz.service;

import java.util.List;
import cn.lijz.domain.CaiGouChuXingZhuShouHangBan;

/**
 * 采购出行助手航班Service接口
 * 
 * @author lijz
 * @date 2024-04-25
 */
public interface ICaiGouChuXingZhuShouHangBanService 
{
    /**
     * 查询采购出行助手航班
     * 
     * @param id 采购出行助手航班主键
     * @return 采购出行助手航班
     */
    public CaiGouChuXingZhuShouHangBan selectCaiGouChuXingZhuShouHangBanById(Long id);

    /**
     * 查询采购出行助手航班列表
     * 
     * @param caiGouChuXingZhuShouHangBan 采购出行助手航班
     * @return 采购出行助手航班集合
     */
    public List<CaiGouChuXingZhuShouHangBan> selectCaiGouChuXingZhuShouHangBanList(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan);

    /**
     * 新增采购出行助手航班
     * 
     * @param caiGouChuXingZhuShouHangBan 采购出行助手航班
     * @return 结果
     */
    public int insertCaiGouChuXingZhuShouHangBan(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan);

    /**
     * 修改采购出行助手航班
     * 
     * @param caiGouChuXingZhuShouHangBan 采购出行助手航班
     * @return 结果
     */
    public int updateCaiGouChuXingZhuShouHangBan(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan);

    /**
     * 批量删除采购出行助手航班
     * 
     * @param ids 需要删除的采购出行助手航班主键集合
     * @return 结果
     */
    public int deleteCaiGouChuXingZhuShouHangBanByIds(Long[] ids);

    /**
     * 删除采购出行助手航班信息
     * 
     * @param id 采购出行助手航班主键
     * @return 结果
     */
    public int deleteCaiGouChuXingZhuShouHangBanById(Long id);
}
