package cn.lijz.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.lijz.mapper.CaiGouChuXingZhuShouHangBanMapper;
import cn.lijz.domain.CaiGouChuXingZhuShouHangBan;
import cn.lijz.service.ICaiGouChuXingZhuShouHangBanService;

/**
 * 采购出行助手航班Service业务层处理
 * 
 * @author lijz
 * @date 2024-04-25
 */
@Service
public class CaiGouChuXingZhuShouHangBanServiceImpl implements ICaiGouChuXingZhuShouHangBanService 
{
    @Autowired
    private CaiGouChuXingZhuShouHangBanMapper caiGouChuXingZhuShouHangBanMapper;

    /**
     * 查询采购出行助手航班
     * 
     * @param id 采购出行助手航班主键
     * @return 采购出行助手航班
     */
    @Override
    public CaiGouChuXingZhuShouHangBan selectCaiGouChuXingZhuShouHangBanById(Long id)
    {
        return caiGouChuXingZhuShouHangBanMapper.selectCaiGouChuXingZhuShouHangBanById(id);
    }

    /**
     * 查询采购出行助手航班列表
     * 
     * @param caiGouChuXingZhuShouHangBan 采购出行助手航班
     * @return 采购出行助手航班
     */
    @Override
    public List<CaiGouChuXingZhuShouHangBan> selectCaiGouChuXingZhuShouHangBanList(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        return caiGouChuXingZhuShouHangBanMapper.selectCaiGouChuXingZhuShouHangBanList(caiGouChuXingZhuShouHangBan);
    }

    /**
     * 新增采购出行助手航班
     * 
     * @param caiGouChuXingZhuShouHangBan 采购出行助手航班
     * @return 结果
     */
    @Override
    public int insertCaiGouChuXingZhuShouHangBan(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        caiGouChuXingZhuShouHangBan.setCreateTime(DateUtils.getNowDate());
        return caiGouChuXingZhuShouHangBanMapper.insertCaiGouChuXingZhuShouHangBan(caiGouChuXingZhuShouHangBan);
    }

    /**
     * 修改采购出行助手航班
     * 
     * @param caiGouChuXingZhuShouHangBan 采购出行助手航班
     * @return 结果
     */
    @Override
    public int updateCaiGouChuXingZhuShouHangBan(CaiGouChuXingZhuShouHangBan caiGouChuXingZhuShouHangBan)
    {
        caiGouChuXingZhuShouHangBan.setUpdateTime(DateUtils.getNowDate());
        return caiGouChuXingZhuShouHangBanMapper.updateCaiGouChuXingZhuShouHangBan(caiGouChuXingZhuShouHangBan);
    }

    /**
     * 批量删除采购出行助手航班
     * 
     * @param ids 需要删除的采购出行助手航班主键
     * @return 结果
     */
    @Override
    public int deleteCaiGouChuXingZhuShouHangBanByIds(Long[] ids)
    {
        return caiGouChuXingZhuShouHangBanMapper.deleteCaiGouChuXingZhuShouHangBanByIds(ids);
    }

    /**
     * 删除采购出行助手航班信息
     * 
     * @param id 采购出行助手航班主键
     * @return 结果
     */
    @Override
    public int deleteCaiGouChuXingZhuShouHangBanById(Long id)
    {
        return caiGouChuXingZhuShouHangBanMapper.deleteCaiGouChuXingZhuShouHangBanById(id);
    }
}
