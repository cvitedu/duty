package cn.lijz.service;

import java.util.List;
import cn.lijz.domain.CaiGouChuXingZhuShou;

/**
 * 采购出行助手Service接口
 * 
 * @author lijz
 * @date 2024-04-21
 */
public interface ICaiGouChuXingZhuShouService 
{
    /**
     * 查询采购出行助手
     * 
     * @param id 采购出行助手主键
     * @return 采购出行助手
     */
    public CaiGouChuXingZhuShou selectCaiGouChuXingZhuShouById(Long id);

    /**
     * 查询采购出行助手列表
     * 
     * @param caiGouChuXingZhuShou 采购出行助手
     * @return 采购出行助手集合
     */
    public List<CaiGouChuXingZhuShou> selectCaiGouChuXingZhuShouList(CaiGouChuXingZhuShou caiGouChuXingZhuShou);

    /**
     * 新增采购出行助手
     * 
     * @param caiGouChuXingZhuShou 采购出行助手
     * @return 结果
     */
    public int insertCaiGouChuXingZhuShou(CaiGouChuXingZhuShou caiGouChuXingZhuShou);

    /**
     * 修改采购出行助手
     * 
     * @param caiGouChuXingZhuShou 采购出行助手
     * @return 结果
     */
    public int updateCaiGouChuXingZhuShou(CaiGouChuXingZhuShou caiGouChuXingZhuShou);

    /**
     * 批量删除采购出行助手
     * 
     * @param ids 需要删除的采购出行助手主键集合
     * @return 结果
     */
    public int deleteCaiGouChuXingZhuShouByIds(Long[] ids);

    /**
     * 删除采购出行助手信息
     * 
     * @param id 采购出行助手主键
     * @return 结果
     */
    public int deleteCaiGouChuXingZhuShouById(Long id);
}
